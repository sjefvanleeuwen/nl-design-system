// Karma configuration
// Generated on Wed May 04 2016 14:07:46 GMT+0200 (CEST)

module.exports = function (config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '../',

    detectBrowsers: {
      enabled: true,
      usePhantomJS: false
    },

    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: [
      'detectBrowsers',
      'jasmine',
      'karma-typescript'
    ],

    plugins: [
      'karma-chrome-launcher',
      'karma-detect-browsers',
      'karma-firefox-launcher',
      'karma-jasmine',
      'karma-ie-launcher',
      'karma-safari-launcher',
      'karma-typescript',
      'karma-chrome-launcher'
    ],

    preprocessors: {
      './src/**/*.ts': ['karma-typescript'],
      './test/**/*.spec.ts': ['karma-typescript'],
      './testutils/**/*.ts': ['karma-typescript']
    },

    // list of files / patterns to load in the browser
    files: [
      {
        pattern: './src/**/*.+(js|ts)'
      },
      './test/**/*.spec.ts',
      './testutils/**/*.ts'
    ],

    // list of files to exclude
    exclude: [
      '**/node_modules/*',
      'docs'
    ],

    reporters: [
      'progress',
      'karma-typescript'
    ],

    karmaTypescriptConfig: {
      compilerOptions: {
        allowJs: true,
        // inlineSourceMap: true,
        inlineSources: true,
        outDir: ".tmp/",
        sourceMap: true,
        target: 'ES5', // (optional) Specify ECMAScript target version: 'ES3' (default)
        module: 'commonjs', // (optional) Specify module code generation: 'commonjs' or 'amd',
        lib: ["es2015", "dom"]
      },
      exclude: [
        "node_modules",
        "_docs",
        "docs",
      ],
    },

    // web server port
    port: 9876,

    // enable / disable colors in the output (reporters and logs)
    colors: true,

    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,

    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,

    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: [
      'Safari',
      'FirefoxHeadless',
      'ChromeHeadless'
    ],

    customLaunchers: {
        FirefoxHeadless: {
            base: 'Firefox',
            flags: [ '-headless' ],
        },
    },

    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: 0
  })
};
