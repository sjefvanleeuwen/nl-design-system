"use strict";

/**
 * Gets the parent url
 */
hexo.extend.helper.register('parent', (path) => {
  let parts = path.split('/'),
    page = parts.pop();
  if(page === '' || page === 'index.html') {
    // Url ended with a slash, so remove another portion
    parts.pop();
  }
  return parts.length > 1 ? parts.join('/') : '/';
});
