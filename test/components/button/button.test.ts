import { Button } from '../../../src/components/button/button';
import { KeyboardEvents, KEYCODE_SPACE } from '../../../testutils/KeyboardEvents';

describe('Button component', () => {

    let buttonStub = `<a href="#btn" class="btn">link</a>`,
        element: HTMLElement,
        button: HTMLElement;

    beforeEach(() => {
        element = document.createElement('div');
        element.innerHTML = buttonStub;
        button = element.querySelector('a') as HTMLElement;
        document.body.appendChild(element);
    });

    it('should not be disabled if styled as link', () => {
        new Button(button);
        button.classList.add('btn--link');
        expect(button.getAttribute('disabled')).toBeUndefined;
        expect(button).toMatchSnapshot();
    });

    it('should add a tabindex if button is not disabled', () => {
        new Button(button);
        expect(button.getAttribute('tabindex')).toEqual('0');
        expect(button).toMatchSnapshot();
    });

    it('should not add a tabindex if button is disabled', () => {
        button.classList.add('btn--disabled');
        new Button(button);
        expect(button.hasAttribute('tabindex')).toEqual(false);
        expect(button).toMatchSnapshot();
    });

    it('should add an aria role', () => {
        new Button(button);
        expect(button.getAttribute('role')).toEqual('button');
        expect(button).toMatchSnapshot();
    });

    it('should follow the href on space', () => {
        new Button(button);
        let evt = jest.spyOn(button, 'click');
        KeyboardEvents.KeyDown(button, KEYCODE_SPACE);
        expect(evt).toHaveBeenCalled();
    });

    afterEach(() => {
        document.body.removeChild(element);
    });

});