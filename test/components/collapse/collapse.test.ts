import { Collapse } from '../../../src/components/collapse/collapse';
import { Events } from '../../../testutils/Events';

describe('Collapse component', () => {
    let element: HTMLElement;

    const TEMPLATE_BASED_ON_X_UNO: string = `<div class="collapse">
        <div class="collapse__title" x-uno-collapse-target="target">Titel van collapse</div>
        <div class="collapse__details" id="target">
            Humani generiss sunt dominas de nobilis compater.
            Cliniass cadunt in audax cirpi! Est placidus urbs, ces aris.
        </div>
    </div>`;

    const TEMPLATE_BASED_ON_DOM_STRUCTURE: string = `<div class="collapse">
        <div class="collapse__title" x-uno-collapse>Titel van collapse</div>
        <div class="collapse__details">
            Humani generiss sunt dominas de nobilis compater.
            Cliniass cadunt in audax cirpi! Est placidus urbs, ces aris.
        </div>
    </div>`;

    const TEMPLATE_COLLAPSE_GROUP_BASED_ON_ID: string = `<div class="collapse">
        <div class="collapse__title" x-uno-collapse x-uno-collapse-group="test">Titel van collapse</div>
        <div class="collapse__details">
            Humani generiss sunt dominas de nobilis compater.
            Cliniass cadunt in audax cirpi! Est placidus urbs, ces aris.
        </div>
        <div class="collapse__title" x-uno-collapse x-uno-collapse-group="test">Titel van collapse</div>
        <div class="collapse__details">
            Humani generiss sunt dominas de nobilis compater.
            Cliniass cadunt in audax cirpi! Est placidus urbs, ces aris.
        </div>
    </div>`;

    const TEMPLATE_COLLAPSE_GROUP_BASED_ON_DOM_STRUCTURE: string = `<div class="collapse" x-uno-collapse-group>
        <div class="collapse__title" x-uno-collapse ="test">Titel van collapse</div>
        <div class="collapse__details">
            Humani generiss sunt dominas de nobilis compater.
            Cliniass cadunt in audax cirpi! Est placidus urbs, ces aris.
        </div>
        <div class="collapse__title" x-uno-collapse>Titel van collapse</div>
        <div class="collapse__details">
            Humani generiss sunt dominas de nobilis compater.
            Cliniass cadunt in audax cirpi! Est placidus urbs, ces aris.
        </div>
    </div>`;

    function getElement(tpl: string): HTMLElement {
        element.innerHTML = tpl;
        return element;
    }

    beforeEach(() => {
        element = document.createElement('div');
    });

    it('should check if the collapse has a target', () => {
        let el: Element = document.createElement('div'),
            thrown: string = '';
        try {
            new Collapse(el);
        } catch (e) {
            thrown = e.message;
        }
        expect(thrown).toBe('Collapse cannot find collapse target');
    });

    it('should check if the collapse target exists', () => {
        let el: HTMLElement = document.createElement('div'),
            thrown: string = '';
        el.setAttribute('x-uno-collapse-target', 'non-existent');
        try {
            new Collapse(el);
        } catch (e) {
            thrown = e.message;
        }
        expect(thrown).toBe('Collapse cannot find collapse target');
    });

    it('should set the target based on the dom structure', () => {
        document.body.appendChild(getElement(TEMPLATE_BASED_ON_DOM_STRUCTURE));
        let thrown: string = '';
        try {
            new Collapse(element.querySelector('[x-uno-collapse]'));
        } catch (e) {
            thrown = e.message;
        }
        expect(thrown).not.toBe('Collapse cannot find collapse target');
    });


    it('should set the target based on [x-uno-collapse-target]', () => {
        document.body.appendChild(getElement(TEMPLATE_BASED_ON_X_UNO));
        let thrown: string = '';
        try {
            new Collapse(element.querySelector('[x-uno-collapse]'));
        } catch (e) {
            thrown = e.message;
        }
        expect(thrown).not.toBe('Collapse cannot find collapse target');
    });

    it('should set an id to the details if not present', () => {
        document.body.appendChild(getElement(TEMPLATE_BASED_ON_DOM_STRUCTURE));
        new Collapse(element.querySelector('[x-uno-collapse]'));
        expect(document.querySelector('.collapse__details').hasAttribute('id')).toBe(true);
    });

    it('should not set an id to the details if it already has an id', () => {
        document.body.appendChild(getElement(TEMPLATE_BASED_ON_X_UNO));
        let id: string = document.querySelector('.collapse__details').getAttribute('id');
        new Collapse(element.querySelector('[x-uno-collapse]'));
        expect(document.querySelector('.collapse__details').getAttribute('id')).toBe(id);
    });

    it('should set aria attributes to the details', () => {
        document.body.appendChild(getElement(TEMPLATE_BASED_ON_X_UNO));
        new Collapse(element.querySelector('[x-uno-collapse-target]'));
        expect(document.getElementById('target').getAttribute('aria-expanded')).toBe('false');
    });

    it('should set aria attributes to the title', () => {
        document.body.appendChild(getElement(TEMPLATE_BASED_ON_X_UNO));
        new Collapse(element.querySelector('[x-uno-collapse-target]'));
        expect(element.querySelector('[x-uno-collapse-target]').getAttribute('role')).toBe('button');
        expect(element.querySelector('[x-uno-collapse-target]').getAttribute('tabindex')).toBe('0');
        expect(element.querySelector('[x-uno-collapse-target]').getAttribute('aria-controls')).toBe('target');
    });

    it('should only initialize once', () => {
        document.body.appendChild(getElement(TEMPLATE_BASED_ON_X_UNO));
        new Collapse(element.querySelector('[x-uno-collapse-target]'));
        // remove an attribute set by the collapse
        element.querySelector('[x-uno-collapse-target]').removeAttribute('role');
        // it should not be set again, since the collapse is already initialized
        new Collapse(element.querySelector('[x-uno-collapse-target]'));
        expect(element.querySelector('[x-uno-collapse-target]').hasAttribute('role')).toBe(false);
        expect(element.querySelector('[x-uno-collapse-target]').hasOwnProperty('unoCollapse')).toBe(true);
    });

    it('should remove event listeners on destroy', () => {
        document.body.appendChild(getElement(TEMPLATE_BASED_ON_X_UNO));
        let el: HTMLElement = element.querySelector('[x-uno-collapse-target]') as HTMLElement;
        let collapse: Collapse = new Collapse(el);
        let spy = spyOn(collapse, 'toggle');
        el.click();
        collapse.destroy();
        el.click();
        expect(spy).toHaveBeenCalledTimes(1);
    });

    describe('grouping', () => {

        it('should fire an event when the collapse is opened', (done) => {
            document.body.appendChild(getElement(TEMPLATE_COLLAPSE_GROUP_BASED_ON_ID));
            let toggle: HTMLElement = (element.querySelector('[x-uno-collapse]') as HTMLElement);
            toggle.addEventListener('collapse-open', onCollapseOpen);
            new Collapse(element.querySelectorAll('[x-uno-collapse]').item(0));
            new Collapse(element.querySelectorAll('[x-uno-collapse]').item(1));
            toggle.click();
            function onCollapseOpen() {
                toggle.removeEventListener('collapse-open', onCollapseOpen);
                done();
            }
        });

        it('should close a collapse when another collapse in the same group is opened based on group id', () => {
            document.body.appendChild(getElement(TEMPLATE_COLLAPSE_GROUP_BASED_ON_ID));
            let el1: HTMLElement = (element.querySelectorAll('[x-uno-collapse]').item(0) as HTMLElement);
            let el2: HTMLElement = (element.querySelectorAll('[x-uno-collapse]').item(1) as HTMLElement);
            let collapse = new Collapse(el1);
            let spy = spyOn(collapse, 'close');
            new Collapse(el2);
            el1.click();
            el2.click();
            expect(spy).toHaveBeenCalledTimes(1);
        });

        it('should close a collapse when another collapse in the same group is opened based on enclousing group id', () => {
            document.body.appendChild(getElement(TEMPLATE_COLLAPSE_GROUP_BASED_ON_DOM_STRUCTURE));
            let el1: HTMLElement = (element.querySelectorAll('[x-uno-collapse]').item(0) as HTMLElement);
            let el2: HTMLElement = (element.querySelectorAll('[x-uno-collapse]').item(1) as HTMLElement);
            let collapse = new Collapse(el1);
            let spy = spyOn(collapse, 'close');
            new Collapse(el2);
            el1.click();
            el2.click();
            expect(spy).toHaveBeenCalledTimes(1);
        });
    });

    it('should call toggle on click', () => {
        document.body.appendChild(getElement(TEMPLATE_BASED_ON_X_UNO));
        let collapse = new Collapse(element.querySelector('[x-uno-collapse-target]'));
        let spy = spyOn(collapse, 'toggle');
        (element.querySelector('[x-uno-collapse-target]') as HTMLElement).click();
        expect(spy).toHaveBeenCalledTimes(1);
    });

    it('should call toggle on enter if the title has focus', () => {
        document.body.appendChild(getElement(TEMPLATE_BASED_ON_X_UNO));
        let collapse = new Collapse(element.querySelector('[x-uno-collapse-target]'));
        let spy = spyOn(collapse, 'toggle');
        Events.KeyDown(element.querySelector('[x-uno-collapse-target]'), 13);
        expect(spy).toHaveBeenCalledTimes(1);
    });

    it('should call toggle on space if the title has focus', () => {
        document.body.appendChild(getElement(TEMPLATE_BASED_ON_X_UNO));
        let collapse = new Collapse(element.querySelector('[x-uno-collapse-target]'));
        let spy = spyOn(collapse, 'toggle');
        Events.KeyDown(element.querySelector('[x-uno-collapse-target]'), 32);
        expect(spy).toHaveBeenCalledTimes(1);
    });

    it('should not call toggle on any keyboard key except space or enter', () => {
        document.body.appendChild(getElement(TEMPLATE_BASED_ON_X_UNO));
        let collapse = new Collapse(element.querySelector('[x-uno-collapse-target]'));
        let spy = spyOn(collapse, 'toggle');
        Events.KeyDown(element.querySelector('[x-uno-collapse-target]'), 33);
        expect(spy).not.toHaveBeenCalled();
    });

    it('should add the toggle class when the collapse is clicked', () => {
        document.body.appendChild(getElement(TEMPLATE_BASED_ON_X_UNO));
        let details = document.getElementById('target');
        new Collapse(element.querySelector('[x-uno-collapse-target]'));
        (element.querySelector('[x-uno-collapse-target]') as HTMLElement).click();
        expect(details.classList.contains('collapse__details--open')).toBe(true);
    });

    it('should fire a collapse-open event if the collapse is opened', () => {
        document.body.appendChild(getElement(TEMPLATE_BASED_ON_X_UNO));
        const collapse: Collapse = new Collapse(element.querySelector('[x-uno-collapse-target]'));
        const spy = spyOn(collapse, 'open');
        expect(spy).not.toHaveBeenCalled();
        collapse.open();
        expect(spy).toHaveBeenCalledTimes(1);
    });
    
    it('should fire a collapse-close event if the collapse is closed', () => {
        document.body.appendChild(getElement(TEMPLATE_BASED_ON_X_UNO));
        const collapse: Collapse = new Collapse(element.querySelector('[x-uno-collapse-target]'));
        const spy = spyOn(collapse, 'close');
        collapse.open();
        expect(spy).not.toHaveBeenCalled();
        collapse.close();
        expect(spy).toHaveBeenCalledTimes(1);
    });

    afterEach(() => {
        if (element.parentElement) {
            element.parentNode.removeChild(element);
        }
    });
});