import {} from 'jasmine';
import {Utils} from '../../src/core/utils';

describe('Core Utils', () => {
    // Uid = uno-xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
    // Where x is an alphanumeric character
    let pattern = new RegExp("uno-[a-z0-9]{8}(-[a-z0-9]{4}){3}-[a-z0-9]{12}");

    it('should generate an id', () => {
        // Setup
        // Execute
        let id = Utils.GenerateUID();
        // Test
        expect(pattern.test(id)).toBe(true);
    });

    it('should generate an unique id', () => {
        // Setup
        // Execute
        let id1 = Utils.GenerateUID(),
            id2 = Utils.GenerateUID();
        // Test
        expect(id1).not.toBe(id2);
    });

    it('should return the nearest parent element containing the specified class of an element', () => {
        // Setup
        const STRUCTURE = '<div class="tab__tab" id="result"><div><div><a href="#"></a></div></div>';
        let el:HTMLElement = document.createElement('div');
        el.innerHTML = STRUCTURE;

        // Test
        let result:Element = Utils.FindParentContainingClass(el.querySelector('a'), 'tab__tab', el);

        // Execute
        expect(result.getAttribute('id')).toBe('result')
    });

    it('should return the null when a parent containing the specified class of an element was not found', () => {
        // Setup
        const STRUCTURE = '<div class="tab__tab" id="result"><div><div><a href="#"></a></div></div>';
        let el:HTMLElement = document.createElement('div');
        el.innerHTML = STRUCTURE;

        // Test
        let result:Element = Utils.FindParentContainingClass(el.querySelector('a'), 'non-existent-class', el);

        // Execute
        expect(result).toBe(null);
    });

});
