export const KEYCODE_ESC: number = 27;
export const KEYCODE_TAB: number = 9;
export const KEYCODE_ENTER: number = 13;
export const KEYCODE_LEFT: number = 37;
export const KEYCODE_UP: number = 38;
export const KEYCODE_RIGHT: number = 39;
export const KEYCODE_DOWN: number = 40;
export const KEYCODE_SPACE: number = 32;

export const KEYMAP: any = {
    9: 'Tab',
    13: 'Enter',
    27: 'Escape',
    32: ' ',
    37: 'ArrowLeft',
    38: 'ArrowUp',
    39: 'ArrowRight',
    40: 'ArrowDown'
};

export class KeyboardEvents {

    public static KeyDown(element:any, code:any):void {
        KeyboardEvents.dispatch(element, code, 'keydown');

    }
    public static KeyUp(element:any, code:any):void {
        KeyboardEvents.dispatch(element, code, 'keyup');
    }

    /**
     * Based on http://output.jsbin.com/awenaq/3
     * @param element
     * @param code
     * @param type
     */
    private static dispatch(element:any, code:any, type:any):void {
        // @ts-ignore
        const eventObj:any = document.createEventObject ?
            // @ts-ignore
            document.createEventObject() : document.createEvent('Events');
        if (eventObj.initEvent){
            eventObj.initEvent(type, true, true);
        }

        eventObj.keyCode = code;
        eventObj.which = code;
        eventObj.key = KEYMAP.hasOwnProperty(code) ? KEYMAP[code] : String.fromCharCode(code);

        // @ts-ignore
        element.dispatchEvent ? element.dispatchEvent(eventObj) : element.fireEvent(`on${type}`, eventObj);
    }
}
