module.exports = {
  dev: {
    src: ['src/**/!(*spec).ts'],
    // Output to _docs folder instead of docs, so Hexo's browsersync will pick up changes
    dest: '_docs/themes/duo-ux/source/uno',
    options: {
      module: 'commonjs',
      target: 'es5',
      sourceMap: true,
      inlineSources: true,
      rootDir: 'src',
      declaration: true,
      alwaysStrict: true,
      noUnusedParameters: true,
      noUnusedLocals: true
    }
  },

  dist: {
    src: ['src/**/!(*spec).ts'],
    dest: 'dist',
    options: {
      module: 'commonjs',
      target: 'es5',
      sourceMap: false,
      rootDir: 'src',
      declaration: true,
      alwaysStrict: true,
      noUnusedParameters: true,
      noUnusedLocals: true
    }
  },

  watch: {
    src: ['src/**/!(*spec).ts'],
    // Output to _docs folder instead of docs, so Hexo's browsersync will pick up changes
    dest: '_docs/themes/duo-ux/source/uno',
    options: {
      module: 'commonjs',
      target: 'es5',
      sourceMap: true,
      rootDir: 'src',
      declaration: true,
      watch: true,
      alwaysStrict: true,
      noUnusedParameters: true,
      noUnusedLocals: true
    }
  }
};
