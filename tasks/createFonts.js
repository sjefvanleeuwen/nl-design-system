module.exports = (grunt) => {
    "use strict";
  
    // Builds a new version of the icon font.
    grunt.registerTask('font', 'Build a new icon font', [
      'webfont:generate',
    ]);
  };