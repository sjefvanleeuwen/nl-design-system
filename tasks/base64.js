/**
 * Created by idsklijnsma on 31/08/16.
 */
const SVGO = require('svgo'),
  fs = require('fs');

module.exports = (grunt) => {
  "use strict";

  grunt.registerTask('base64', 'Base64 encodes a file', function() {
    let done = this.async();
    let file = grunt.option('file');
    let svgo = new SVGO();

    fs.readFile(file, 'utf8', (err,data) => {
      if (err) {
        throw err;
      }

      svgo.optimize(data, (result) => {
        let encoded = new Buffer(result.data).toString('base64');
        console.log(`${encoded}`);
        done();
      });
    });
  });
};