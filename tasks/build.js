module.exports = (grunt) => {
  "use strict";

  // Builds a new version of the library. Because the a11y tests are running against compiled code,
  // We need to build before we can run action tests
  grunt.registerTask('build', 'Build a fresh version of the library', [
    'clean:docs',
    'clean:dist',
    'clean:uno',
    'copy:dist',
    'typescript:dist',
    'uglify:dist',
    'sass:dist',
    'postcss:dist', // Build css
    'concat:dist',
    'concat:tridion',
    'usebanner:dist',
    'copy:uno'
  ]);
};