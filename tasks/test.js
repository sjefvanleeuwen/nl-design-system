'use strict';

module.exports = (grunt) => {

  grunt.registerTask('test:unittest', 'Run unittests', ['tslint:dev']);

  grunt.registerTask('test:unitwatch', 'Run unittests', ['karma:watch']);

  grunt.registerTask('test:webrichtlijnen', 'Run accessibility tests', ['rsids_pa11y:docs']);

  grunt.registerTask('test:a11y', 'Run accessibility tests', ['rsids_pa11y']);

  grunt.registerTask('test:visual', 'Run visual tests', ['copy:dev', 'docs:generate', 'visual:test']);

  grunt.registerTask('test', 'Run all tests', [
    'tslint', // Build js
    'postcss:lint', 'sass:dev', 'postcss:dev', // Build css to dev for a11y
    'copy:dev', 'docs:generate',
    // 'test:visual',
    'test:unittest',
    'test:webrichtlijnen'
  ]);
};

